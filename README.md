# Heroku app:
- api: https://app-challenge2-api.herokuapp.com

**Continuous Integration (CI) gerando o build das imagens  docker no registry do gitlab, fazendo o deploy automaticamente para o heroku e aplicando as migrations**


# Iniciando os serviços no Docker:
    ~/challenge2$ docker-compose build --build-arg configuration=""
    ~/challenge2$ docker-compose up -d 
    
    acessar api:  http://localhost:8000/

# Manualmente - back-end python 3.8 + django
    configure as variáveis do banco de dados no arquivo services/api/db.env
    com as variáveis do seu ambiente:
    *** o postgresql já deve estar instalado e o banco de dados criado. ***    
        DB_NAME: api
        DB_USER: postgres
        DB_PASS: 123qwe
        DB_HOST: 192.168.0.19
        DB_PORT: 5432
    
    Requisito: Python 3.8
    Abra o terminal, entre na pasta do back-end e execute os comandos:
        ~/challenge2/services/api$ pip install -r requirements.txt
        ~/challenge2/services/api$ python manage.py migrate
        ~/challenge2/services/api$ python manage.py runserver
        
    Para acessar a aplicação http://localhost:8000/
    
> O Api root está listando as rotas padrão e esta desabilitado a autenticação.