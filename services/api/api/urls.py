from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers, permissions

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from apps.city.views import CityViewSet
from apps.holiday.views import HolidayViewSet
from apps.states.views import StatesViewSet

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

router = routers.DefaultRouter()
router.register(r'states', viewset=StatesViewSet)
router.register(r'city', viewset=CityViewSet)
router.register(r'holiday', viewset=HolidayViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('feriados/<int:state_or_city>/<int:year>-<int:month>-<int:day>/', HolidayViewSet.as_view(
        {'get': 'retrieve', 'post': 'create', 'put': 'create', 'delete': 'destroy'}), name='feriados'),
    path('feriados/<int:state_or_city>/<int:month>-<int:day>/', HolidayViewSet.as_view(
        {'get': 'retrieve', 'post': 'create', 'put': 'create', 'delete': 'destroy'}), name='feriados'),
    path('feriados/<int:state_or_city>/<str:name>/',
         HolidayViewSet.as_view({'post': 'create', 'put': 'create', 'delete': 'destroy', }), name='feriados'),

    url(r'^(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
