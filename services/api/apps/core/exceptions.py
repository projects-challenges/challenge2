from django.http import JsonResponse
from django.db import IntegrityError
from rest_framework.exceptions import APIException
from rest_framework import status, views
from rest_framework.response import Response


def server_error(request, *args, **kwargs):
    """
    Generic 500 error handler.
    """
    data = {
        'detail': ''
    }
    return JsonResponse(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def exception_handler(exc, context):
    response = views.exception_handler(exc, context)
    if isinstance(exc, IntegrityError):
        from psycopg2 import errorcodes as pg_errorcodes
        if exc.__cause__.pgcode == pg_errorcodes.FOREIGN_KEY_VIOLATION:
            return Response({'message': exc.__cause__.pgerror},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        elif exc.__cause__.pgcode == pg_errorcodes.UNIQUE_VIOLATION:
            return Response({'message': exc.__cause__.pgerror},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({'detail': exc.__cause__.pgerror},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    if isinstance(exc, BaseAPIException):
        response.data['args'] = exc.args_list
    return response


class BaseAPIException(APIException):
    def __init__(self, message=None):
        self.args_list = []


class AlreadyException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_409_CONFLICT
        if message == None:
            self.detail = ''
        else:
            self.detail = message


class AlreadyUsedException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_409_CONFLICT


class AlreadyUsedExceptionWarning(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_412_PRECONDITION_FAILED


class NoRecordsProcessedException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_400_BAD_REQUEST
        self.detail = ''


class DeleteException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_403_FORBIDDEN
        self.detail = ''


class NoContentException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_204_NO_CONTENT
