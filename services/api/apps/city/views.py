# Create your views here.
from rest_framework import viewsets
from apps.city.models import City
from apps.city.serializers import CitySerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer

    def list(self, request, *args, **kwargs):
        self.queryset = City.objects.prefetch_related(
            'state__city_set'
        )
        self.serializer_class = CitySerializer
        return super(CityViewSet, self).list(request, *args, **kwargs)
