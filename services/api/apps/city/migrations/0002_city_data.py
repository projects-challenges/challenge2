# Generated by Django 3.1.1 on 2020-09-11 02:39

from django.db import migrations
import csv


def import_data(apps, schema_editor):
    city = apps.get_model('city', 'City')
    with open('media/municipios.csv', 'r', encoding='utf-8') as cv:
        data = csv.reader(cv, delimiter=',')
        for row in data:
            new_city = city(id=row[0], state_id=row[0][0:2], name=row[1])
            new_city.save()


class Migration(migrations.Migration):
    dependencies = [
        ('city', '0001_initial'),
        ('states', '0002_states_data'),
    ]

    operations = [
        migrations.RunPython(import_data),
    ]
