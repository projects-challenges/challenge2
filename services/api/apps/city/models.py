from django.db import models
from django.db.models import Model

from apps.core.models import ModelBase
from apps.states.models import States


class City(ModelBase):
    id = models.IntegerField(primary_key=True)
    state = models.ForeignKey(States, models.DO_NOTHING)
    name = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        managed = True
        db_table = 'city'
