from rest_framework import serializers
from apps.city.models import City
from apps.states.serializers import StatesSerializer


class CitySerializer(serializers.HyperlinkedModelSerializer):
    state_obj = StatesSerializer(read_only=True, source="states")

    class Meta:
        model = City
        fields = '__all__'
