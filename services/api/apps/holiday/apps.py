from django.apps import AppConfig


class HolidayConfig(AppConfig):
    name = 'apps.holiday'
