from rest_framework import serializers
from apps.holiday.models import Holiday
from apps.states.serializers import StatesSerializer


class HolidaySerializer(serializers.ModelSerializer):
    state_obj = StatesSerializer(read_only=True, source="state")

    class Meta:
        model = Holiday
        fields = '__all__'


class HolidayLightSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Holiday
        fields = ['name']
