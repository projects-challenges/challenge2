from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

# Create your tests here.
from rest_framework.utils import json


class HolidayTests(APITestCase):
    def test_holiday_labor_day(self):
        response = self.client.get('/feriados/1600501/2020-05-01/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {'name': 'Dia do Trabalhador'})

        response = self.client.get('/feriados/4305439/2020-05-01/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {'name': 'Dia do Trabalhador'})

    def test_put_holiday_national(self):
        response = self.client.delete('/feriados/4305439/05-01/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
