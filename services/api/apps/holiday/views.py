# Create your views here.

from rest_framework import viewsets, status
from rest_framework.response import Response

from apps.holiday import actions
from apps.holiday.models import Holiday
from apps.holiday.serializers import HolidaySerializer


class HolidayViewSet(viewsets.ModelViewSet):
    queryset = Holiday.objects.all()
    serializer_class = HolidaySerializer

    def list(self, request, *args, **kwargs):
        self.queryset = Holiday.objects.prefetch_related(
            'state__city_set',
            'city__state__city_set'
        )
        self.serializer_class = HolidaySerializer
        return super(HolidayViewSet, self).list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        if kwargs.get('state_or_city') and kwargs.get('year') and kwargs.get('month') and kwargs.get('day'):
            return actions.find(**kwargs)
        return super(HolidayViewSet, self).retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(HolidayViewSet, self).update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        data_create_update = actions.valid_create_update(request, **kwargs)
        created_updated = Holiday.objects.create(**data_create_update)
        if created_updated:
            return Response(status=status.HTTP_200_OK)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def destroy(self, request, *args, **kwargs):
        if kwargs.get('pk'):
            return super(HolidayViewSet, self).destroy(self, request, *args, **kwargs)

        self.id = None
        result = Holiday.delete(self)

        if result and result[0]:
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(status=status.HTTP_403_FORBIDDEN)
