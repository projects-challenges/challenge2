import datetime
from dateutil import easter
from rest_framework import status
from rest_framework.response import Response
from apps.holiday import choices
from apps.holiday.models import Holiday


def corpus_christi_date(year):
    date_easter = easter.easter(year)
    date_corpus_christi = datetime.date.fromordinal(date_easter.toordinal() + 60)
    return date_corpus_christi


def carnival_date(year):
    date_easter = easter.easter(year)
    date_carnival = datetime.date.fromordinal(date_easter.toordinal() - 47)
    return date_carnival


def friday_of_passion(year):
    date_easter = easter.easter(year)
    date_friday_of_passion = datetime.date.fromordinal(date_easter.toordinal() - 2)
    return date_friday_of_passion


def check_date_corpus_christi(year, month, day):
    date_corpus_christi = corpus_christi_date(year)
    date_check = datetime.date(year, month, day)

    if date_check == date_corpus_christi:
        return True

    return False


def check_date_carnival(year, month, day):
    date_carnival = carnival_date(year)
    date_check = datetime.date(year, month, day)

    if date_check == date_carnival:
        return True

    return False


def check_date_friday_of_passion(year, month, day):
    date_friday_of_passion = friday_of_passion(year)
    date_check = datetime.date(year, month, day)

    if date_check == date_friday_of_passion:
        return True

    return False


def find(**kwargs):
    state_or_city = kwargs.get('state_or_city')
    year = kwargs.get('year')
    month = kwargs.get('month')
    day = kwargs.get('day')

    if state_or_city and datetime.date(year, month, day):
        if check_date_friday_of_passion(year, month, day):
            return Response(data={'name': choices.HOLIDAY_MOVABLES[0][1]}, status=status.HTTP_200_OK)

        holiday = Holiday.objects.get_holiday(state_or_city=state_or_city, month=month, day=day)
        if holiday.count() > 0:
            return Response(data={'name': holiday.first().name}, status=status.HTTP_200_OK)
        else:
            if check_date_corpus_christi(year, month, day) or check_date_carnival(year, month, day):
                movable = Holiday.objects.get_movable(city_id=state_or_city,
                                                      name=[choices.CORPUS_CHRISTI, choices.CARNIVAL])
                if movable.count() > 0:
                    return Response(data={'name': movable.first().get_movable_name_display()},
                                    status=status.HTTP_200_OK)

    return Response(status=status.HTTP_404_NOT_FOUND)


def valid_create_update(request, *args, **kwargs):
    name = request.data.get('name') if kwargs.get('name') is None else kwargs.get('name')
    day = request.data.get('day') if kwargs.get('day') is None else kwargs.get('day')
    month = request.data.get('month') if kwargs.get('month') is None else kwargs.get('month')
    city = request.data.get('city') if kwargs.get('state_or_city') is None else kwargs.get('state_or_city')
    state = request.data.get('state') if kwargs.get('state_or_city') is None else kwargs.get('state_or_city')

    data_create_update = {
        'name': name,
        'day': day,
        'month': month,
        'city_id': city,
        'state_id': state
    }
    return data_create_update
