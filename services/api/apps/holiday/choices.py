FRIDAY_OF_PASSION = 'friday-of-passion'
CORPUS_CHRISTI = 'corpus-christi'
CARNIVAL = 'carnaval'

HOLIDAY_MOVABLES = (
    (FRIDAY_OF_PASSION, 'Sexta-Feira Santa'),
    (CORPUS_CHRISTI, 'Corpus Christi'),
    (CARNIVAL, 'Carnaval',)
)