from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Model, Q
from apps.core.exceptions import AlreadyException, DeleteException, NoRecordsProcessedException
from apps.city.models import City
from apps.core.models import ModelBase
from apps.holiday import choices
from apps.states.models import States


class HolidayQuerySet(models.QuerySet):
    def get_national(self, month, day):
        return self.filter(month=month, day=day, city__id__isnull=True, state__id__isnull=True)

    def get_state(self, month, day, state_id):
        return self.filter(month=month, day=day, state_id=state_id)

    def get_city(self, month, day, city_id):
        return self.filter(month=month, day=day, city_id=city_id)

    def get_holiday(self, state_or_city, month, day):
        return self.filter(name__isnull=False). \
            filter(Q(month=month, day=day) | Q(state__id=state_or_city) | Q(city__id=state_or_city) | Q(
            state__city__id=state_or_city)).distinct()

    def get_movable(self, city_id, name):
        return self.filter(city_id=city_id, movable=True, movable_name__in=name)


class HolidayManager(models.Manager):
    def create(self, **obj_data):
        return super().create(**obj_data)


class Holiday(ModelBase):
    id = models.BigAutoField(primary_key=True)
    month = models.IntegerField(
        validators=[
            MaxValueValidator(12),
            MinValueValidator(1),
        ], null=True)
    day = models.IntegerField(
        validators=[
            MaxValueValidator(31),
            MinValueValidator(1)
        ], null=True)
    state = models.ForeignKey(States, models.DO_NOTHING, related_name='states', null=True)
    city = models.ForeignKey(City, models.DO_NOTHING, related_name='city', null=True)
    name = models.TextField(null=True)
    movable = models.BooleanField(default=False)
    movable_name = models.TextField(choices=choices.HOLIDAY_MOVABLES, null=True)

    objects = HolidayManager.from_queryset(HolidayQuerySet)()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        try:
            if self.name in [choices.CORPUS_CHRISTI, choices.CARNIVAL]:
                if City.objects.filter(pk=self.city_id).count() == 0:
                    raise AlreadyException()

                self.movable = True
                if Holiday.objects.get_movable(self.city_id, [self.name]).count() > 0:
                    raise AlreadyException()

                self.state_id = None
                self.month = None
                self.day = None
                self.movable_name = self.name
                self.name = None
                return super(Holiday, self).save(*args, **kwargs)
            elif self.pk is None:
                if States.objects.filter(pk=self.state_id).count() > 0:
                    self.state_id = States.objects.filter(pk=self.state_id).first().id
                    self.city_id = None
                elif City.objects.filter(pk=self.city_id).count() > 0:
                    self.state_id = None
                    self.city_id = City.objects.filter(pk=self.city_id).first().id
                else:
                    self.state_id = None
                    self.city_id = None

                state_or_city = self.city_id if self.state_id is None else self.state_id
                holiday = Holiday.objects.get_holiday(state_or_city=state_or_city, month=self.month, day=self.day)

                if holiday.count() > 0:
                    return holiday.update(name=self.name)
            return super(Holiday, self).save(*args, **kwargs)
        except Holiday.DoesNotExist as error:
            raise NoRecordsProcessedException(error)

    def delete(self, *args, **kwargs):
        try:
            if self.id:
                return super(Holiday, self).delete()

            name = self.kwargs.get('name')
            month = self.kwargs.get('month')
            day = self.kwargs.get('day')
            state_or_city = self.kwargs.get('state_or_city')
            if name and name in [choices.CORPUS_CHRISTI, choices.CARNIVAL]:
                movable = Holiday.objects.get_movable(city_id=state_or_city, name=[name])
                if movable.count() > 0:
                    self = movable.first()

            holiday = Holiday.objects.get_holiday(state_or_city=state_or_city, month=month, day=day)
            state = States.objects.filter(pk=state_or_city)
            city = City.objects.filter(pk=state_or_city)
            if state.count() > 0 and holiday.first().state_id != state_or_city:
                raise DeleteException()
            elif city.count() > 0 and holiday.first().city_id != state_or_city:
                raise DeleteException()
            else:
                self = holiday.first()
            return super(Holiday, self).delete()
        except Holiday.DoesNotExist:
            raise DeleteException()

    class Meta:
        managed = True
        db_table = 'holiday'
