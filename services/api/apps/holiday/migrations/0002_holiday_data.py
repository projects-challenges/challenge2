# Generated by Django 3.1.1 on 2020-09-11 02:39

from django.db import migrations

HOLIDAYS = [
    {
        'day': 1,
        'month': 1,
        'description': 'Ano Novo',
    },
    {
        'day': 21,
        'month': 4,
        'description': 'Tiradentes',
    },
    {
        'day': 1,
        'month': 5,
        'description': 'Dia do Trabalhador',
    },
    {
        'day': 7,
        'month': 9,
        'description': 'Independência',
    },
    {
        'day': 12,
        'month': 10,
        'description': 'Nossa Senhora Aparecida',
    },
    {
        'day': 2,
        'month': 11,
        'description': 'Finados',
    },
    {
        'day': 15,
        'month': 11,
        'description': 'Proclamação da República',
    },
    {
        'day': 25,
        'month': 12,
        'description': 'Natal',
    },
]


def import_data(apps, schema_editor):
    holiday = apps.get_model('holiday', 'Holiday')
    for row in HOLIDAYS:
        new_holiday = holiday(day=row['day'], month=row['month'], name=row['description'])
        new_holiday.save()


class Migration(migrations.Migration):
    dependencies = [
        ('holiday', '0001_initial'),
        ('states', '0002_states_data'),
        ('city', '0002_city_data'),
    ]

    operations = [
        migrations.RunPython(import_data),
    ]
