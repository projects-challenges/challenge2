from rest_framework import serializers
from apps.states.models import States


class StatesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = States
        fields = ['id', 'code']
