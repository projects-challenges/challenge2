from django.db import models

from apps.core.models import ModelBase


class States(ModelBase):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=2)

    def __str__(self):
        return self.code

    class Meta:
        managed = True
        db_table = 'states'
