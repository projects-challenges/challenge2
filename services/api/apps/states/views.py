# Create your views here.
from rest_framework import viewsets
from apps.states.models import States
from apps.states.serializers import StatesSerializer


class StatesViewSet(viewsets.ModelViewSet):
    queryset = States.objects.all()
    serializer_class = StatesSerializer
