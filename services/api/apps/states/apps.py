from django.apps import AppConfig


class StatesConfig(AppConfig):
    name = 'apps.states'
